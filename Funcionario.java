public class Funcionario {
	
	String rg;	
	String nome;
	double salario;
	String departamento;
	Data dataDeEntrada;
	
	public void bonifica(double bonus){
		salario+=bonus;
	}
	
	public double calculaGanhoAnual(){
		return salario*12;
	}

	public void mostra(){
	
		System.out.println("Nome do funcionario: " + nome);
		System.out.println("RG: " + rg);
		System.out.println("Data de Entrada: " + dataDeEntrada.mostraData());
		System.out.println("Departamento: " + departamento);
		System.out.println("Salário: " + salario);
		System.out.println("Salário anual: " + calculaGanhoAnual());
	}
	
	
}


