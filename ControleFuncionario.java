import java.util.Scanner;

public class ControleFuncionario {
	public static void main(String[] args){
	
	Scanner input = new Scanner(System.in);
	
	
	/*Funcionario f2 = new Funcionario();
	f2.nome="Danilo";
	f2.salario=100;
	
	Funcionario f3 = new Funcionario(); //4. Embora f2 tenha os mesmos atributos de f3, eles são objetos diferentes então a saída do 
	f3.nome="Danilo";				//código será "diferentes".
	f3.salario=100;
	
	Funcionario f3 = f2;	//5. Quando são criadas duas referências para o mesmo objeto, a saída do código será "iguais"
	
	if(f2==f3)
		System.out.println("iguais");
	else
		System.out.println("diferentes");*/
	
	Funcionario f1 = new Funcionario();
	
	System.out.print("Insira o nome do funcionario: ");
	f1.nome=input.nextLine();
	
	System.out.print("Insira o RG do funcionario: ");
	f1.rg=input.nextLine();
	
	f1.dataDeEntrada = new Data();
	
	System.out.print("Insira o dia de entrada do funcionario: ");
	f1.dataDeEntrada.dia = input.nextInt();
	
	System.out.print("Insira o mes de entrada do do funcionario: ");
	f1.dataDeEntrada.mes = input.nextInt();
	
	System.out.print("Insira o ano de entrada do funcionario: ");
	f1.dataDeEntrada.ano = input.nextInt();
	
	input.nextLine(); //esvaziar o buffer do teclado
	
	System.out.print("Insira o departamento do funcionario: ");
	f1.departamento=input.nextLine();
	
	
	System.out.print("Insira o salario do funcionario: ");
	f1.salario=input.nextDouble();
	
	System.out.print("Insira a bonificacao do funcionario: ");
	double bonus = input.nextDouble();
	f1.bonifica(bonus);
	
	f1.mostra();
	
	}
		

}

